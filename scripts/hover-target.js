export function hoverTarget() {
    $("#chat-log").on('mouseover', '.targetPicker', function () {
        $(this).css("background-color", "yellow");
    });

    $("#chat-log").on('dblclick', '.targetPicker', function (e) {
        let base = this;
        
        $(this).parent().children(".targetPicker").each(function () {
            if ($(base).attr("data-hitType") === $(this).attr("data-hitType")) {
                $(this).finish().fadeOut(40).fadeIn(40);
                (canvas.tokens.placeables.find(t => t.data._id === $(this).attr("data-target"))).control({ releaseOthers: false });
            }
        });
    })

    $("#chat-log").on('click', '.targetPicker', function (e) {
        $(this).finish().fadeOut(40).fadeIn(40);
        const target = getTarget($(this).attr("data-target"));
        
        if (target._controlled) {
            if (e.shiftKey || canvas.tokens.controlled.length === 1) { 
                target.release();
            } else {
                target.control({ releaseOthers: true });
            }
        } else {
            target.control({ releaseOthers: !e.shiftKey });
        }
    });

    $("#chat-log").on('mouseout', '.targetPicker', function () {
        $(this).css("background-color", "transparent");
    });
}

function getTarget(dataId) {
    return canvas.tokens.placeables.find(t => t.data._id === dataId);
}
