const StatusTextsByStep = {
	0: { "hitType": "cm", "symbol": "💔", "color": "#990000" },
	1: { "hitType": "m", "symbol": "❌", "color": "#131516"  },
	2: { "hitType": "h", "symbol": "✔️", "color": "#131516" },
	3: { "hitType": "ch", "symbol": "💥", "color": "#4C7D4C"  }
};

const SaveMessagesByStep = {
    0: { "message": "Critically failed", "chatMessage": "💔 Crit Fail" },
    1: { "message": "Failed", "chatMessage": "❌ Fail" },
    2: { "message": "Succeeded", "chatMessage": "✔️ Success" },
    3: { "message": "Critically succeeded", "chatMessage": "💥 Crit Success" }
}

const AttackMessagesByStep = {
    0: { "message": "Critically missed", "chatMessage": "💔 Crit Miss" },
    1: { "message": "Missed", "chatMessage": "❌ Miss" },
    2: { "message": "Hit", "chatMessage": "✔️ Hit" },
    3: { "message": "Critically hit", "chatMessage": "💥 Crit Hit" }
}

export function generateChatMessage(token, successStep, successBy, isAttack) {
    const status = StatusTextsByStep[successStep];
    const messagesByStep = isAttack ? AttackMessagesByStep : SaveMessagesByStep;
    const message = messagesByStep[successStep];

	if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(token, message.chatMessage, { emote: true });
	
	return `
			<div class="targetPicker" data-target="${token.data._id}" data-hitType="${status.hitType}">
				<div style="color:#131516;margin-top:4px;">
					${status.symbol} <b>${token.name}:</b>
				</div>
				<div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
					${status.symbol}
				<b style="color:${status.color}">
					${message.message}${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}` : ``)}${successStep == 0 || successStep == 3 ? '!' : '.'}
				</b>
				</div>
			</div>`;
}
