import {hoverTarget} from "./scripts/hover-target.js";
import {compareAttacks} from "./scripts/attack-roller.js";
import {forceDCSave} from "./scripts/save-roller.js";
import {AbilityTemplate} from "./scripts/abilityTemplate.js";
import {showResults} from "./scripts/show-results.js";

//Clamp Function, pretty handy:
Math.clamp = function (number, min, max) {
    return Math.max(min, Math.min(number, max));
}

//Save DC Button Creation:
function createDCButton(content) {
	var html = $('<div />', { html: content });
	html.find('button[data-action="save"]').attr('data-action', 'saveDC');
	return html.html();
}

Hooks.on("createChatMessage", (message) => {
    if (game.user.data._id === game.users.contents.find(u => u.isGM).data._id || game.settings.get("pf2qr", "PlayersCanQuickRoll")) {
        if (game.combat === null || (game.user.data._id === game.users.contents.find(u => u.isGM).data._id) || !game.settings.get("pf2qr", "RollOnTurn") || (game.combat.combatant.players[0]?.data._id === game.user.data._id)) {

            if (!message.data) return true;
            if (!message.data.flavor) return true;
            if (message.data.flavor.includes('Attack Roll') && message.data.user === game.user.data._id) compareAttacks(message);
            if (message.data.flavor.includes('Strike:') && message.data.user === game.user.data._id) compareAttacks(message);
        }
    }
});

// Handle-socket
Hooks.on('ready', () => {
    console.log("Registering...")
    game.socket.on('module.pf2qr', async (data) => {
        if (game.user.isGM && data) showResults(data);
      });
    })

//Pre-Create:
Hooks.on("preCreateChatMessage", (data) => {
    let content = data.data._source.content;
    var saveButtons = $(content).find('button[data-action="save"]')
    if (!game.settings.get("pf2qr", "SystemSaving") && saveButtons.length) content = createDCButton(content);

    //add the template button if on an older version
    let systemVersion = game.system.data.version
    let splitVersion = systemVersion.split('.')
    if (splitVersion[0] <= 2 && splitVersion[1] <= 11) {
        if (content.includes(`Area:`)) content = addTemplateButton(content);
    }
    data.data._source.content = content;
});

Hooks.on('renderChatLog', (log, html) => {
    
    hoverTarget();

    html.on('click', '.card-buttons button', (ev) => {
        const button = $(ev.currentTarget);
        const dc = ev.currentTarget.innerText.match(/\d+/g)
        const action = button.attr('data-action');
        const actionSave = button.attr('data-save');

        if (action.includes('saveDC')) forceDCSave(actionSave, dc);
        if (action.includes('template')) createTemplate(button);
    });
});

//Creating of the template buttons in chat log:
function addTemplateButton(content) {
    let split = content.split('\n');

    let range = 0;
    let shape = "ERROR";
	
	let templates = {};

    for (let index = 0; index < split.length; index++) {
        const element = split[index];
        if (element.includes("Area:")) {
			let matchGroups = element.match(/(\d+).?(?:foot|ft).(\w+)/ig);
			matchGroups.forEach(group =>{
				let data = group.match(/(\d+).?(?:foot|ft).(\w+)/i);
				range = data[1];
				switch(data[2].toLowerCase()){
					case 'emanation':
                    case 'burst':
						shape='circle';
						break;
					case 'cone':
						shape="cone";
						break;
					case 'line':
						shape='ray';
						break;
				}
				templates["<div class='card-buttons'><button data-action='template' class='templateButton' data-area='"+range+"' data-shape='"+shape+"'>Place a "+range+"ft "+shape+"</button></div>"] = 0;
			})
        }
    }
    split = split.join('\n');
	for(const template in templates){
		split += '\n' + template
	}
	return split;
}

var pf2qrGhostTemplate = null;

function createTemplate(button){
    const range = (button[0].attributes['data-area'].value);
    const type = (button[0].attributes['data-shape'].value);
    console.log(type);

    if(pf2qrGhostTemplate!=null) pf2qrGhostTemplate.kill();
    const templateData = {
        t: type,
        user: game.user.id,
        x: 1000,
        y: 1000,
        direction: 180,
        angle: 90,
        distance: parseInt(range),
        borderColor: "#FF0000",
        fillColor: "#FF3366",
    };
    if(type==="ray"){
        templateData.width=5;
    }
    if(type==="rect"){
        templateData.direction=45;
        templateData.width=range;
        templateData.distance=Math.hypot(range,range);
    }
    
    const doc = new MeasuredTemplateDocument(templateData, {parent: canvas.scene});

    pf2qrGhostTemplate = new AbilityTemplate(doc);
    pf2qrGhostTemplate.drawPreview();
}

Hooks.on('ready', () => {
    [
        {
            name: "ShowPlayersResults",
            hint: "Whether players should see the results of rolls. Private/Blind rolls will serve a similar function soon(TM).",
            scope: "world",
            default: true,
            type: Boolean
        }, {
            name: "ShowExceedsBy",
            hint: "Whether to show how much a roll exceeded the AC/DC for.",
            scope: "world",
            default: true,
            type: Boolean
        }, {
            name: "PlayersCanQuickRoll",
            hint: "If disabled, only the GM can quick roll.",
            scope: "world",
            default: true,
            type: Boolean
        }, {
            name: "RollOnTurn",
            hint: "Players can only roll on their turn in combat. Can roll whenever if not in combat.",
            scope: "world",
            default: false,
            type: Boolean
        }, {
             name: "StopTargetingAfterSave",
             hint: "After quick saves are rolled, releases target on tokens.",
             scope: "world",
             default: false,
             type: Boolean
        }, {
            name: "ShowBubbles",
            hint: "Shows bubbles above the heads of tokens for their success. Keep in mind this shows the results to players, though not the exceed by value.",
            scope: "world",
            default: true,
            type: Boolean
        }, {
            name: "SystemSaving",
            hint: "Use the default pathfinder saving instead of PF2QR target saving.",
            scope: "world",
            default: false,
            type: Boolean
        }, {
            name: "RollDamageOnHit",
            hint: "Automatically rolls damage for any Strike that hits.",
            scope: "world",
            default: false,
            type: Boolean
        }, {
            name: "OnlyAutoRollGmDamage",
            hint: "If enabled, automatic damage rolling from strikes will only happen when it is the GM making the strike.",
            scope: "world",
            default: true,
            type: Boolean
        }
    ].forEach((setting) => {
        let options = {
            name: setting.name,
            hint: setting.hint,
            scope: setting.scope,
            config: true,
            default: setting.default,
            type: setting.type,
        };
        game.settings.register("pf2qr", setting.name, options);
    });
});
